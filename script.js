// class WhackAMole {
//   constructor () {
// this.size = size;

//   }
  
// }

const size = 7;
const table = document.querySelector("table"); 
const stringTd = '<td class="field"></td>'.repeat(size);
let stringTrItem = `<tr>${stringTd}</tr>`;
let stringTr = stringTrItem.repeat(size);
table.insertAdjacentHTML("beforeend", stringTr);


const gameArea = document.querySelector('.game-area');
const btnStart = document.querySelector('.score-area__btn--start');
const levelForm = document.querySelector('.level-form');
console.log(levelForm);
const ground = document.querySelectorAll('td');






function getRandomIntInclusive(min, max) {
    do {
        min = Math.ceil(min);
        max = Math.floor(max);
        numberMole = Math.floor(Math.random() * (max - min)) + min; 
    } while (ground[numberMole].classList != 'field');
  }




function startsGame (pauseMole, timeMole) {
  let count = 1;
  let timerId = setInterval(function() {
	showingMole();
    
    console.log(count);
    
     if (count === Math.round(Math.pow(size, 2) / 2)) {
      // if (count === Math.round(Math.pow(size, 2))) {
        clearInterval(timerId);
        table.classList.add('table-hidden')
        if (score >= (count / 2 + 1)) {
          const resultWin = document.querySelector('.result-winner').classList.add('result--visible');
          resultWin = document.querySelector('.result').classList.remove('result--hidden');
          console.log('Ты победил!');
        }else {
          const resultLost = document.querySelector('.result-lost').classList.add('result--visible');
          resultLost = document.querySelector('.result').classList.remove('result--hidden');
          console.log('Ты проиграл!');
        }
        console.log('stop');
    }
    return count++;

}, pauseMole);



  function closingMole() {
    ground[numberMole].classList.remove('js-mole-active');
    ground[numberMole].classList.add('missed');
  }
  
  


function showingMole() {
    getRandomIntInclusive(0, Math.pow(size, 2));
    ground[numberMole].classList.remove('field');
    ground[numberMole].classList.add('js-mole-active');
    setTimeout(closingMole, timeMole);
    
    }
    
}


    const spanScoreText = document.querySelector('.score');
    spanScoreText.hidden = true
    const spanScore = document.querySelector('.score-count');
    let score = 1;
    
gameArea.addEventListener('click', function(event) {
  
    if (event.target.classList.contains("js-mole-active")) {
    event.target.classList.add('caught');
    event.target.classList.remove('js-mole-active');
    console.log('score', score);
     spanScore.innerHTML = score;
    return score++;
     };
     });

btnStart.addEventListener('click', function() {
  
  if (document.querySelector('input[name="level"]:checked').value === 'Middle') {
    startsGame(1000, 800);
  }
  if (document.querySelector('input[name="level"]:checked').value === 'Easy') {
    startsGame(1800, 1200);
  }
  if (document.querySelector('input[name="level"]:checked').value === 'Hard') {
    startsGame(800, 500);
  }
  //startsGame();
  spanScoreText.hidden = false;
  btnStart.hidden = true;
  levelForm.hidden = true;

  
})
  

// // Whack a Mole Game

// // This code is based on code found at
// // https://javascript30.com

// // Create the Whack-a-Mole Game
// // Class

// class WhackAMole {
	
// 	// Properties used to initialize our
// 	// Whack-a-Mole Game
// 	constructor(startButton, moles, scoreOut, gameTimeLength, peepTimeMin, peepTimeMax){		
// 		// Game HTML Elements
// 		this.btnStart = startButton;
// 		this.moles = moles;
// 		this.scoreOut = scoreOut;
		
// 		// Game Images
// 		this.moleImgSrc = 'https://res.michaelwhyte.ca/mole.png';
// 		this.moleBonkedImg = new Image();
// 		this.moleBonkedImg.src = 'https://res.michaelwhyte.ca/mole-whacked.png';
		
// 		// Game Parameters
// 		this.gameTime = gameTimeLength;
// 		this.minPeepTime = peepTimeMin;
// 		this.maxPeepTime = peepTimeMax;
// 		this.numOfMoles = this.moles.length;
		
// 		// Game State Variables
// 		this.prevMoleNumber = null;
// 		this.timeUp = false;
// 		this.score = 0;
// 		this.gameTimer = null;
// 		this.peepTimer = null;		
// 	}
	
// 	init(){
// 		this.score = 0;
// 		this.scoreOut.text(this.score);
// 		this.timeUp = false;
// 		this.prevMoleNumber = null;
// 		this.btnStart.text('Stop Game');
// 		this.peep();
// 		this.gameTimer = setTimeout(() => {
// 			console.log('Game Over...');
// 			this.btnStart.text('Start Game');
// 			this.timeUp = true;
// 		}, this.gameTime);		
// 	}
	
// 	stop(){
// 		console.log('Game Stopped...');
// 		this.btnStart.text('Start Game');
// 		this.timeUp = true;
// 		this.moles.removeClass('up');
// 		clearInterval(this.peepTimer);
// 		clearInterval(this.gameTimer);
// 	}
	
// 	peep(){
// 		  const time = this._randomTime(this.minPeepTime, this.maxPeepTime);
//     	const mole = this._randomMole(this.moles);
//     	mole.addClass('up');
//     	this.peepTimer = setTimeout(() => {
//       		mole.removeClass('up');
//       		if(this.timeUp === false){
// 				    this.peep();
// 			    } 
//     	}, time);
// 	}
	
// 	bonk(mole) {
// 		mole.attr('src', this.moleBonkedImg.src)
//     	  .removeClass('up')
// 		    .addClass('bonked')
// 		    .one('transitionend', () => {
// 				  mole.attr('src', this.moleImgSrc);
// 				  mole.removeClass('bonked');
// 			  });
// 		this.score++;
//     this.scoreOut.text(this.score);
// 	}
	
// 	// Utility functions
	
// 	// generate a random time to determine how long
// 	// the moles stay up
// 	_randomTime(min, max){
// 		return Math.round(Math.random() * (max - min) + min);
// 	}
	
// 	// randomly selects one of the moles to display
// 	_randomMole(moles) {
//     	const idx = Math.floor(Math.random() * this.numOfMoles);
//     	const mole = moles.eq(idx);
//     	if(idx === this.prevMoleNumber ) {
//       		console.log('...same mole...try again...');
//       		return this._randomMole(moles);
//     	}
// 		  this.prevMoleNumber = idx;
//     	return mole;
// 	}
	
// }

// // Get a new instance of the Whack A Mole
// // class
// // Parameters:
// // 1. Start Button
// // 2. Mole Image
// // 3. Score out
// // 4. Game Time Length (ms)
// // 5. Peep Time Min (ms)
// // 6. Peep Time Max (ms)
// const wam = new WhackAMole($('#btn-start'), $('.mole-pic'), $('#score-out'), 12000, 1000, 2000);

// // Game Event Handlers
// wam.btnStart.click(function(){
	
// 	if(wam.btnStart.text() === 'Start Game'){
// 		wam.init();
// 	}else{
// 		wam.stop();
// 	}
	
// });

// wam.moles.click(function(){
	
// 	if($(this).hasClass('bonked')){
// 		return;
// 	}
	
// 	wam.bonk( $(this) );
	
// });
